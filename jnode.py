import sys

import bitcoin
import lnd
import system_act
import user_in

global TOR_SETUP

def main():
    # check python version
    if system_act.python_ver():
        logging.info('Python3 detected')
    else:
        logging.error('Incorrect Python version. Python 3 or superior required')
        user_in.color_text.error('Python 3 or newer needed, try to run with python3')
        sys.exit()
    # check if it is the firs run
    if system_act.first_time():
        logging.info('First run detected')
        user_in.welcome()
        RPC_user = user_in.user_input.RPC_user()
        RPC_pass = user_in.user_input.RPC_pass()
        LND_name = user_in.user_input.LND_name()
        LND_Pass = user_in.user_input.LND_wallet_pass()
        RTL_Pass = user_in.user_input.RTL_pass()
        TOR_SETUP = user_in.user_input.tor_setup()
        DOWN_CHAIN = user_in.user_input.download_chain()
        PRUNE_VALUE = bitcoin.pruning_value() * 1000
        user_in.color_text.info('\nThank you. This is the information I will use\n')
        user_in.color_text.resume(RPC_user + ' will be your RPC user')
        user_in.color_text.resume(LND_name + ' will be your Lightning node name')
        user_in.color_text.resume('Detected ' + str(bitcoin.microsd_size()) + ' GB microsd, node pruning will set to ' + str(bitcoin.pruning_value()) + ' GB')
        if TOR_SETUP == 'y':
            user_in.color_text.resume('TOR will be installed')
        if DOWN_CHAIN == 'y':
            user_in.color_text.resume('Blockchain will be downloaded')
        input("Press Enter to continue...")
        # Create folder
        system_act.create_soft_folder()
        # Create services --> Created before prerequisites because we copy the files in that step
        system_act.create_service.bitcoind()
        if TOR_SETUP == 'y':
            system_act.create_service.lnd_tor()
        else:
            system_act.create_service.lnd()
        system_act.create_service.RTL()
        # Install prerequisites
        system_act.sudo_call(system_act.prerequisites)
        system_act.add_PATH_GO()
        # Install TOR
        if TOR_SETUP == 'y':
            system_act.sudo_call(system_act.TOR_INSTALL)
        # Install Bitcoin Core
        bitcoin.Install_Bitcoin_Core()
        # Add Bitcoin Core bin folder to path
        logging.info('Add bitcoind to path')
        bitcoin.add_PATH_Bitcoin()
        logging.info('bitcoind added to path')
        #Download Blockchain if needed
        if DOWN_CHAIN == 'y':
            logging.info('Downloading bitcoin data folder')
            bitcoin.download_blockchain()
            logging.info('bitcoin data folder done')
        logging.info('Generating bitcoin.conf file')
        user_in.color_text.info('Generating bitcoin.conf file')
        if TOR_SETUP == 'y':
            bitcoin.Bitcoin_conf_file_TOR(RPC_user, RPC_pass, PRUNE_VALUE)
        else:
            bitcoin.Bitcoin_conf_file_no_tor(RPC_user, RPC_pass, PRUNE_VALUE)
        user_in.color_text.info('bitcoin.conf file generated')
        logging.info('bitcoin.conf file generated')
        logging.info('Starting Bitcoin')
        user_in.color_text.info('Starting Bitcoin daemon')#        system_act.sudo_call(bitcoin.BTC_START)
        user_in.color_text.info('Bitcoin Core client started')
        # Install Loopd (Can't install for now)
        #logging.info('Installing Loop')
        #lnd.install_Loopd()
        #logging.info('Loop installed')
        # Install LND
        lnd.Install_LND()
        # Add Bitcoin Core bin folder to path
        logging.info('Add LND to path')
        lnd.add_PATH_LND()
        logging.info('LND added to path')
        logging.info('Generating lnd.conf file')
        user_in.color_text.info('Generating lnd.conf file')
        if TOR_SETUP == 'y':
            lnd.LND_conf_file_TOR(RPC_user, RPC_pass, LND_name)
        else:
            lnd.LND_conf_file_no_TOR(RPC_user, RPC_pass, LND_name)
        user_in.color_text.info('lnd.conf file generated')
        logging.info('lnd.conf file generated')
        logging.info('Starting LND')
        user_in.color_text.info('Starting LND daemon')
        system_act.sudo_call(lnd.lnd_start)
        user_in.color_text.info('LND client started')
        user_in.color_text.info('Installing RTL')
        logging.info('Installing RTL')
        lnd.Install_RTL()
        lnd.Configure_RTL(RTL_Pass)
        system_act.sudo_call(lnd.RTL_start)
        logging.info('Creating LND wallet')
        lnd.create_wallet(LND_Pass)
        user_in.color_text.resume('\nINSTALLATION FINISHED ENTER THIS ADDRESS FROM A EXPLORER:')
        user_in.color_text.resume('\nhttp://'+system_act.lan_ip()+':3000')
        logging.info('THE END')
    else:
        # check arguments
        user_in.color_text.info('Script already installed')
        logging.info('Ya se ha instalado el script')
        sys.exit()

if __name__ == "__main__":
    import logging.config
    logging.basicConfig(format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p', filename='jnode.log',
                        level=logging.DEBUG)
    main()
